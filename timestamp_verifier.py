import requests
import base64
import json
import time

TIMESTAMP_URL_SERVER = 'https://fw2.bry.com.br/api/carimbo-service/v1/timestamps/verify';

HEADER = {
    'Content-Type': 'application/json',
    'Authorization':'Bearer insert-a-valid-token'
}

def verify_token():
    auxAuthorization = HEADER['Authorization'].split(' ')
    if(auxAuthorization[1] == 'insert-a-valid-token'):
        print("Insert a valid token")
        return False
    else:
        return True

def timestamp_report(time_stamp):
    print()

    print('Time stamp status: ', time_stamp['timeStampStatus']['status'])

    print('Time stamp content hash: ', time_stamp['timeStampStatus']['timeStampContentHash'])

    print('Date of time stamp: ', time_stamp['timeStampStatus']['timeStampDate'])

    print('Time stamp serial number: ', time_stamp['timeStampStatus']['timestampSerialNumber'])

    print('Time stamp policy: ', time_stamp['timeStampStatus']['timeStampPolicy'])


def verify_sending_original_document():

# Type 1: Upload and send the original document, accompanied by a time stamp.

    time_stamp_form = {
    'nonce': 1,
    'mode': 'CHAIN',
    'contentReturn': 'true',
    'timestamps': [{'nonce': 1, 'content': base64.b64encode(open('./files/timestamp-teste1.tst','rb').read()).decode("utf-8"), 'documentContent': base64.b64encode(open('./files/logoBRy.png','rb').read()).decode("utf-8")}]
    }
    response = requests.post(TIMESTAMP_URL_SERVER, data= json.dumps(time_stamp_form), headers=HEADER)
    if response.status_code == 200:
        print(" ")

        print('================ Starting time stamp verification by sending original document ... ================')
        print('JSON time stamp verification response: ', response.json())

        timestamp_report(response.json()['reports'][0])
    else:
        print(response.text)

def verify_by_sending_hash_of_the_original_document():

# Type 2: Load the stamp and send it with the hash of the original document.

    time_stamp_form = {
    'nonce': 1,
    'mode': 'CHAIN',
    'contentReturn': 'true',
    'timestamps': [{'nonce': 1, 'content': base64.b64encode(open('./files/timestamp-teste1.tst','rb').read()).decode("utf-8"), 'documentHash': '2CAF281193A7580B88C0A15FE0C9AA7A6E940EA86BB0CC2917EF71229CA49393'}]
    }
    response = requests.post(TIMESTAMP_URL_SERVER, data= json.dumps(time_stamp_form), headers=HEADER)
    if response.status_code == 200:
        print(" ")

        print('============= Starting time stamp verification by hash of the original document ... =============')
        print('JSON time stamp verification response: ', response.json())

        timestamp_report(response.json()['reports'][0])
    else:
        print(response.text)
        
def verify_not_sending_data_from_the_original_document():

# Type 3: Loads and sends only the time stamp.

    time_stamp_form = {
    'nonce': 1,
    'mode': 'CHAIN',
    'contentReturn': 'true',
    'timestamps': [{'nonce': 1, 'content': base64.b64encode(open('./files/timestamp-teste1.tst','rb').read()).decode("utf-8")}]
    }
    response = requests.post(TIMESTAMP_URL_SERVER, data= json.dumps(time_stamp_form), headers=HEADER)
    if response.status_code == 200:
        print(" ")
        print('============= Starting time stamp verification without sending original document data ... =============')

        print('JSON time stamp verification response: ', response.json())

        timestamp_report(response.json()['reports'][0])
    else:
        print(response.text)


if verify_token():
    verify_sending_original_document()
    time.sleep(2)
    verify_by_sending_hash_of_the_original_document()
    time.sleep(2)
    verify_not_sending_data_from_the_original_document()